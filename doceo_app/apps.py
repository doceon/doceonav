from django.apps import AppConfig


class DoceoAppConfig(AppConfig):
    name = 'doceo_app'
