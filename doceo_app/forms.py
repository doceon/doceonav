from django import forms
from doceo_app.models import add_product,full_course

class add_product_form(forms.ModelForm):
    class Meta:
        model=add_product
        fields="__all__"

class add_full_course_form(forms.ModelForm):
    class Meta:
        model=full_course
        fields=["course_category","topic_name","topic_desc","topic_image","topic_video","video_duration"]