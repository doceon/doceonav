from django.shortcuts import render,reverse
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from doceo_app.models import feedback,course_type,register,add_product,cart,order,full_course,cart_course,order,subscribe
from django.contrib.auth.models import User 
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from doceo_app.forms import add_product_form,add_full_course_form
from django.db.models import Q
from datetime import datetime
from django.shortcuts import get_object_or_404
from django.core.mail import EmailMessage
import random
from paypal.standard.forms import PayPalPaymentsForm
from django.conf import settings
from decimal import Decimal
# Create your views here.
def home(request):
    context={}
    if "tutor_id" in request.COOKIES:
        id=request.COOKIES["tutor_id"]
        usr=get_object_or_404(User,id=id)
        login(request,usr)
        return HttpResponseRedirect("/tutor_dashboard")
    if "stu_id" in request.COOKIES:
        id=request.COOKIES["stu_id"]
        usr=get_object_or_404(User,id=id)
        login(request,usr)
        return HttpResponseRedirect("/stu_dashboard")
    check=register.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data=register.objects.get(user__id=request.user.id)
        context["tata"]=data
    feed=register.objects.all()
    context["feed"]=feed
    cour=course_type.objects.all()[:4]
    context["course_t"]=cour
    feed_data=feedback.objects.all()[:5]
    context["messages"]=feed_data
    # print(feed_data)
    if request.method=="POST":
        if "registration" in request.POST:         
            fname=request.POST["fname"]
            lname=request.POST["lname"]
            email=request.POST["email"]
            phone=request.POST["contact"]
            username=request.POST["username"]
            password=request.POST["password"]
            utype=request.POST["utype"]
            #print(request.POST)
            user=User.objects.create_user(username,email,password)
            user.first_name=fname
            user.last_name=lname
            if utype=="tut":
                user.is_staff=True
            user.save()
            reg=register(user=user,phone=phone)
            reg.save()
            return render(request,"home.html",{"status":"{} {} your Account Created Successfully<br>Your Username is {}".format(fname,lname,username)})
        if "changeforgotpwd" in request.POST: 
            un=request.POST["username"]
            pwd=request.POST["newpwd_new"]  

            user=get_object_or_404(User,username=un)
            user.set_password(pwd)
            user.save()
            context["status"]="Password Changed Successfully"
            return render(request,"home.html",context) 
        if "subscribe" in request.POST:
            semail=request.POST["subemail"]
            se=subscribe(email_id=semail)
            se.save()
            
            to=request.POST["subemail"]
            subject="Subscription Of Latest Notifications"
            msg="Dear {} You have successfully subscribed to DOCEO for upcoming latest notifiactions. \n We'll try to give you latest updates as soon as possible. \n Thanks & Regards,\n Navjot Singh, \n From DOCEO.".format(semail)
            try: 
                em=EmailMessage(subject,msg,to=[to,])
                em.send()
                context["status"]="Thanks {},\nYou have subscribed to DOCEO for free Latest Notifications".format(to)
                context["cls"]="success"
            except:
                context["status"]="Email is not Sent"
                context["cls"]="danger"

        if "subs" in request.POST:
            semail=request.POST["subemail"]
            se=subscribe(email_id=semail)
            se.save()
            
            to=request.POST["subemail"]
            subject="Subscription Of Latest Notifications"
            msg="Dear {} You have successfully subscribed to DOCEO for upcoming latest notifiactions. \n We'll try to give you latest updates as soon as possible. \n Thanks & Regards,\n Navjot Singh, \n From DOCEO.".format(semail)
            try: 
                em=EmailMessage(subject,msg,to=[to,])
                em.send()
                context["stat"]="Thanks {},\nYou have subscribed to DOCEO for free Latest Notifications".format(to)
                context["cls"]="success"
            except:
                context["stat"]="Email is not Sent"
                context["cls"]="danger"


    return render(request,"home.html",context)
def check_user(request):
    if request.method=="GET":
        un=request.GET["usern"]
        check =User.objects.filter(username=un)
        
        if len(check)==1:
            return HttpResponse("exists")
        else:
            return HttpResponse("not")
def user_login(request):

    if request.method=="POST":
        un=request.POST["username"]
        pwd=request.POST["password"]

        user=authenticate(username=un, password=pwd)
        if user:
            login(request,user)
            if user.is_superuser:
                return HttpResponseRedirect("/admin")
            if user.is_staff and user.is_active:
                res= HttpResponseRedirect("/tutor_dashboard")
                if "keep_signed_in" in request.POST:
                    res.set_cookie("tutor_id",user.id)
                    res.set_cookie("date_login",datetime.now())
                return res
            if user.is_active:
                res= HttpResponseRedirect("/stu_dashboard")
                if "keep_signed_in" in request.POST:
                    res.set_cookie("stu_id",user.id)
                    res.set_cookie("date_login",datetime.now())
                return res
                
        else:
            return render(request,"home.html",{"invalid_login":"Invalid Username or Password"})
    return HttpResponse("LOGIN CALLED")
@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect("/")
def about(request):
    cour=course_type.objects.all()
    return render(request,"about.html",{"course_t":cour})
def contact(request):
    cour=course_type.objects.all()
    if request.method=="POST":
        fname=request.POST["fname"]
        lname=request.POST["lname"]
        email=request.POST["email"]
        phone=request.POST["phone"]
        message=request.POST["message"]
        data=feedback(fname=fname,lname=lname,email=email,phone=phone,message=message)
        data.save()
        res="Dear {} {} your Feedback Sumitted Succesfully".format(fname,lname)
        return render(request,"contact.html",{"status":res})
        return HttpResponse("data saved successfully")


    return render(request,"contact.html",{"course_t":cour})
def courses(request):
    cour=course_type.objects.all()
    # print(cour)
    return render(request,"courses.html",{"course_t":cour})

    
def courses_detail(request):
    context={}
    cour=course_type.objects.all()
    context["course_t"]=cour
    
    all_courses=full_course.objects.all().order_by("topic_name")
    context["topics"]=all_courses
    if "cat" in request.GET:
        cid=request.GET["cat"]
        cour=course_type.objects.filter(id=cid)
        context["course_typ"]=cour
        prd=full_course.objects.filter(course_category__id=cid)
        context["topicss"]=prd
    return render(request,"courses_detail.html",context)
@login_required
def start_course(request):
    context={}
    if "cat" in request.GET:
        cid=request.GET["cat"]
        cour=course_type.objects.filter(id=cid)
        context["course_typ"]=cour
        prd=full_course.objects.filter(course_category__id=cid)
        context["topicss"]=prd
    return render(request,"start_course.html",context)
def all_courses(request):
    context={}
    all_courses=full_course.objects.all().order_by("topic_name")
    context["topicss"]=all_courses
    if "qry" in request.GET:
        q=request.GET["qry"]
        prd=full_course.objects.filter(Q(topic_name__contains=q)|Q(course_category__course_name__contains=q))
        context["course"]=prd
    if "cat" in request.GET:
        cid=request.GET["cat"]
        prd=full_course.objects.filter(course_category__id=cid)
        context["course"]=prd
    return render(request,"all_courses.html",context)

def all_products(request):
    context={}
    cour=course_type.objects.all()
    context["course_t"]=cour
    all_products=add_product.objects.all().order_by("product_name")
    context["products"]=all_products
    if "qry" in request.GET:
        q=request.GET["qry"]
        prd=add_product.objects.filter(Q(product_name__contains=q)|Q(product_category__course_name__contains=q))
        context["products"]=prd
    if "cat" in request.GET:
        cid=request.GET["cat"]
        prd=add_product.objects.filter(product_category__id=cid)
        context["products"]=prd
    return render(request,"all_products.html",context)


@login_required
def stu_dashboard(request):
    context={}
    check=register.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data=register.objects.get(user__id=request.user.id)
        context["data"]=data
    
    if request.method=="POST":
        if "changepwdsubmit" in request.POST: 
            current_pwd=request.POST["cupwd"]
            new_pwd=request.POST["npwd"]
            
            user=User.objects.get(id=request.user.id)
            un=user.username
            check=user.check_password(current_pwd)
            if check==True:
                user.set_password(new_pwd)
                user.save()
                context["msz"]="Password Changed Successfully"
                context["col"]="alert-success"
                user=User.objects.get(username=un)
                login(request,user)

            else:
                context["msz"]="Incorrect Current Password"
                context["col"]="alert-danger"

        
        if "feedbackinput" in request.POST:
            print(request.POST)
            f=request.POST["feed"]
            data.feedback_input=f
            data.save()
            context["status"]="Your Feedback submitted Successfully"
        if "editprofilesubmit" in request.POST:
            print(request.POST)
            fname=request.POST["fname"]
            lname=request.POST["lname"]
            email=request.POST["email"]
            phone=request.POST["contact"]
            age=request.POST["age"]
            city=request.POST["city"]
            gen=request.POST["gen"]
            education=request.POST["education"]
            current_status=request.POST["current_status"]
            
            usr=User.objects.get(id=request.user.id)
            usr.first_name=fname
            usr.last_name=lname
            usr.email=email
            usr.save()


            data.phone=phone
            data.age=age
            data.city=city
            data.gender=gen
            data.education=education
            data.current_status=current_status
            data.save()
            if "profile_pic" in request.FILES:
                img=request.FILES["profile_pic"]
                data.profile_pic=img
                data.save()

            context["status"]="Thanks Your Profile Updated Succesfully"    
        
    return render(request,"stu_dashboard.html",context)

@login_required
def tutor_dashboard(request):
    context={}
    data=register.objects.get(user__id=request.user.id)
    context["data"]=data
    if request.method=="POST":
        print(request.POST)
        fname=request.POST["fname"]
        lname=request.POST["lname"]
        email=request.POST["email"]
        phone=request.POST["contact"]
        age=request.POST["age"]
        city=request.POST["city"]
        gen=request.POST["gen"]
        education=request.POST["education"]
        current_status=request.POST["current_status"]
        
        usr=User.objects.get(id=request.user.id)
        usr.first_name=fname
        usr.last_name=lname
        usr.email=email
        usr.save()


        data.phone=phone
        data.age=age
        data.city=city
        data.gender=gen
        data.education=education
        data.current_status=current_status
        data.save()
        if "profile_pic" in request.FILES:
            img=request.FILES["profile_pic"]
            data.profile_pic=img
            data.save()

        context["status"]="Thanks Your Profile Updated Succesfully"
    return render(request,"tutor_dashboard.html",context)


@login_required
def stu_dash_backup(request):
    context={}
    data=register.objects.get(user__id=request.user.id)
    context["data"]=data
    if request.method=="POST":
        print(request.POST)
        fname=request.POST["fname"]
        lname=request.POST["lname"]
        email=request.POST["email"]
        phone=request.POST["contact"]
        age=request.POST["age"]
        city=request.POST["city"]
        gen=request.POST["gen"]
        education=request.POST["education"]
        current_status=request.POST["current_status"]
        
        usr=User.objects.get(id=request.user.id)
        usr.first_name=fname
        usr.last_name=lname
        usr.email=email
        usr.save()


        data.phone=phone
        data.age=age
        data.city=city
        data.gender=gen
        data.education=education
        data.current_status=current_status
        data.save()
        if "profile_pic" in request.FILES:
            img=request.FILES["profile_pic"]
            data.profile_pic=img
            data.save()

        context["status"]="Thanks Your Profile Updated Succesfully"

        
    return render(request,"student_backup.html",context)


def add_product_view(request):

    context={}
    check=register.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data=register.objects.get(user__id=request.user.id)
        context["data"]=data
    form =add_product_form()
    if request.method=="POST":
        form=add_product_form(request.POST,request.FILES)
        if form.is_valid():
            data=form.save(commit=False)
            login_user=User.objects.get(username=request.user.username)
            data.seller=login_user
            data.save()
            context["status"]="{} added successfully".format(data.product_name)
    context["form"]=form
    
    return render(request,"add_course.html",context)

def add_full_course(request):
    context={}
    check=register.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data=register.objects.get(user__id=request.user.id)
        context["data"]=data
    form =add_full_course_form()
    if request.method=="POST":
        form=add_full_course_form(request.POST,request.FILES)
        if form.is_valid():
            data=form.save(commit=False)
            login_user=User.objects.get(username=request.user.username)
            data.tutor=login_user
            data.save()
            context["status"]="{} added successfully".format(data.topic_name)
    context["form"]=form
    return render(request,"full_course.html",context)

@login_required
def tutor_courses(request):
    context={}
    all=full_course.objects.filter(tutor__id=request.user.id)
    context["topics"]=all
    
    cat=course_type.objects.all().order_by("course_name")
    context["cats"]=cat
    if request.method=="POST":
        pname=request.POST["pname"]
        pcat=request.POST["pcat"]
        pprice=request.POST["pprice"]
        sprice=request.POST["sprice"]
        pdetails=request.POST["pdet"]

        all.product_name=pname
        all.product_category=pcat
        all.product_price=pprice
        all.sale_price=sprice
        all.details=pdetails
        if "pimg" in request.FILES:
            img=request.POST["pimg"]
            all.product_image=img
        all.save()
        context["status"]="Your Product Updated Successfully"

    return render(request,"tutor_courses.html",context)

@login_required
def single_product(request):
    context={}
    id=request.GET["cid_c"]
    obj=full_course.objects.get(id=id)
    context["product"]=obj
    return render(request,"single_product.html",context)

@login_required
def update_product(request):
    context={}
    id=request.GET["cid_cid"]
    all=full_course.objects.get(id=id)
    context["topics"]=all
    cat=course_type.objects.all().order_by("course_name")
    context["cats"]=cat
    if request.method=="POST":
        pname=request.POST["pname"]
        pcat=request.POST["pcat"]
        pdetails=request.POST["pdet"]
        cat_obj=course_type.objects.get(id=pcat)

        all.topic_name=pname
        all.topic_category=cat_obj
        all.details=pdetails
        if "pimg" in request.FILES:
            img=request.POST["pimg"]
            all.product_image=img
        all.save()
        context["status"]="Your Product Updated Successfully"

    return render(request,"update_product.html",context)

@login_required
def delete_product(request):

    context={}
    if "cid_cid" in request.GET:
        id=request.GET["cid_cid"]
        prd=full_course.objects.get(id=id)
        context["product"]=prd

        if "action" in request.GET:
            prd.delete()
            context["status"]=str(prd.topic_name)+"  Deleted Successfully!!!!!!!"
    return render(request,"delete_product.html",context)


@login_required
def sendemail(request):

    context={}
    if request.method=="POST":
        print(request.POST)
        to=request.POST["toemail"]
        subject=request.POST["subject"]
        msg=request.POST["msg"]
        try: 
            em=EmailMessage(subject,msg,to=[to,])
            em.send()
            context["successmsg"]="Email Sent Successfully"
            context["cls"]="success"
        except:
            context["successmsg"]="Email is not Sent"
            context["cls"]="danger"
    return render(request,"sendemail.html",context)

def add_to_cart_course(request):
    print("HELLO")
    context={}
    items=cart_course.objects.filter(user__id=request.user.id,status=False)
    context["items"]=items
    if request.user.is_authenticated:
        if request.method=="POST":
            pid=request.POST["pid_hidd"]
            qty=request.POST["qtty"]
            print(pid)
            is_exists=cart_course.objects.filter(topic__id=pid,user__id=request.user.id,status=False)
            if len(is_exists)>0:
                context["msz"]="Item already exits in Cart"
                context["cls"]="alert alert-warning"
            else:
                topic_n=get_object_or_404(course_type,id=pid)
                print(topic_n)
                usr=get_object_or_404(User,id=request.user.id)
                cc=cart_course(user=usr,topic=topic_n,quantity=qty)
                cc.save()
                print(cc)
                context["msz"]="{} added to your Cart".format(topic_n.course_name)
                context["cls"]="alert alert-primary"
    else:
        context["status"]="Please Login First"        
    return render(request,"cart_ss.html",context)
def add_to_cart(request):
    context={}
    items=cart.objects.filter(user__id=request.user.id,status=False)
    context["items"]=items
    if request.user.is_authenticated:
        if request.method=="POST":
            pid=request.POST["pid_hidden"]
            qty=request.POST["qty"]
            is_exists=cart.objects.filter(product__id=pid,user__id=request.user.id,status=False)
            if len(is_exists)>0:
                context["msz"]="Item already exits in Cart"
                context["cls"]="alert alert-warning"
            else:
                product=get_object_or_404(add_product,id=pid)
                usr=get_object_or_404(User,id=request.user.id)
                c=cart(user=usr,product=product,quantity=qty)
                c.save()
                context["msz"]="{} added to your Cart".format(product.product_name)
                context["cls"]="alert alert-primary"
    else:
        context["status"]="Please Login First"        
    return render(request,"cart.html",context)

def reset_password(request):
    un=request.GET["username"]
    try:
        user=get_object_or_404(User,username=un)
        otp=random.randint(1000,9999)
        msz="Dear {} \n Your OTP is {}".format(user.username,otp)
        try:
            email=EmailMessage("Password Reset",msz,to=[user.email])
            email.send()
            return JsonResponse({"status":"sent","email":user.email,"rotp":otp})
        except:
            return JsonResponse({"status":"notsent","email":user.email})      
    except:
        return JsonResponse({"status":"notfound"})

def get_cart_data(request):
    items=cart_course.objects.filter(user__id=request.user.id,status=False)
    sale,total,quantity=0,0,0
    for i in items:
        sale += float(i.topic.sale_price)*i.quantity
        total += float(i.topic.course_price)*i.quantity
        quantity += float(i.quantity)
    res={
        "total":total,"offer":sale,"quan":quantity,
    }
    return JsonResponse(res)

def change_quan(request):
    if "quantity" in request.GET:
        cid=request.GET["cid"]
        qty=request.GET["quantity"]
        cart_obj=get_object_or_404(cart,id=cid)
        cart_obj.quantity=qty
        cart_obj.save()
        return HttpResponse(cart_obj.quantity)
    if "remove_prd" in request.GET:
        id=request.GET["remove_prd"]
        cart_obj=get_object_or_404(cart,id=id)
        cart_obj.delete()
        return HttpResponse(1)

def process_payment(request):
    items=cart_course.objects.filter(user__id=request.user.id,status=False)
    products=""
    amt=0
    inv="DOCEO-"
    cartids=""
    cids=""
    for j in items:
        products+=str(j.topic.course_name)+"\n"
        cids+=str(j.topic.id)+","
        amt +=j.topic.sale_price
        inv += str(j.id)
        cartids+=str(j.id)+","
    paypal_dict = {
        'business': settings.PAYPAL_RECEIVER_EMAIL,
        'amount': str(amt),
        'item_name': products,
        'invoice': inv,
        
        'notify_url': 'http://{}{}'.format("127.0.0.1:8000",
                                           reverse('paypal-ipn')),
        'return_url': 'http://{}{}'.format("127.0.0.1:8000",
                                           reverse('payment_done')),
        'cancel_return': 'http://{}{}'.format("127.0.0.1:8000",
                                              reverse('payment_cancelled')),
    }
    usr=User.objects.get(username=request.user.username)
    ord=order(cust_id=usr,cart_ids=cartids,course_ids=cids,invoice_id=inv)
    ord.save()
    request.session["order_id"]=ord.id
    form = PayPalPaymentsForm(initial=paypal_dict)
    return render(request, 'process_payment.html', {'form': form})

def payment_done(request):
    if "order_id" in request.session:
        order_id=request.session["order_id"]
        ord_obj=get_object_or_404(order,id=order_id)
        ord_obj.status=True
        ord_obj.save()
    return render(request,"payment_success.html")

def payment_cancelled(request):
    return render(request,"payment_failed.html")

def careers(request):
    return render(request,"careers.html")
@login_required
def my_courses(request):
    context={}
    check=register.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data=register.objects.get(user__id=request.user.id)
        context["tata"]=data
    my_all_courses=[]
    courses=order.objects.filter(cust_id__id=request.user.id)
    for o in courses:
        minecourses=[]
        
        for id in o.course_ids.split(",")[:-1]:
            print("hello")
            cours=get_object_or_404(course_type,id=id)
            print(cours)
            minecourses.append(cours)
        ord={
            "order_id":o.id,
            "mine_courses":minecourses,
            "invoice":o.invoice_id,
            "status":o.status,
            "date":o.process_on,


        }
        my_all_courses.append(ord)
    context["order_history"]=my_all_courses

    return render(request,"my_courses.html",context)