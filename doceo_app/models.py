from django.db import models
from django.contrib.auth.models import User 
# Create your models here.
class feedback(models.Model):
    fname=models.CharField(max_length=125)
    lname=models.CharField(max_length=125)
    email=models.EmailField(max_length=200)
    phone=models.IntegerField()
    message=models.TextField(blank=True)
    added_on_DT=models.DateTimeField(auto_now_add=True) 
    def __str__(self):
        return self.fname
class course_type(models.Model):
    course_name=models.CharField(max_length=125)
    course_pic=models.ImageField(upload_to="media/course_img")
    description=models.TextField()
    course_price=models.FloatField(null=True)
    sale_price=models.FloatField(null=True)
    learn_spec=models.TextField(null=True)
    intro_video=models.FileField(upload_to="media/course_intro_video",null=True)
    added_on_DT=models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.course_name

class register(models.Model):
    user=models.OneToOneField(User,on_delete=models.CASCADE)
    phone=models.IntegerField()
    profile_pic=models.ImageField(upload_to="profile_pic/%Y/%m/%d",null=True,blank=True)
    age=models.CharField(max_length=250,null=True,blank=True)
    city=models.CharField(max_length=250,null=True,blank=True)
    education=models.TextField(null=True,blank=True)
    current_status=models.CharField(max_length=250,null=True,blank=True)
    gender=models.CharField(max_length=250,default="Male")
    feedback_input=models.TextField(null=True,blank=True)
    added_on_DT=models.DateTimeField(auto_now_add=True,null=True) 
    updated_on=models.DateTimeField(auto_now=True,null=True) 
    def __str__(self):
        return self.user.username

class full_course(models.Model):
    tutor=models.ForeignKey(User,on_delete=models.CASCADE)
    course_category=models.ForeignKey(course_type,on_delete=models.CASCADE)
    topic_name=models.CharField(max_length=250)
    topic_desc=models.TextField()
    topic_image=models.ImageField(upload_to="course_topic/")
    topic_video=models.FileField(upload_to="course_topic_video/")
    video_duration=models.CharField(max_length=250,null=True)
    added_on_DT=models.DateTimeField(auto_now_add=True)
    updated_on=models.DateTimeField(auto_now=True,null=True) 
    def __str__(self):
        return self.tutor.username  
        
class add_product(models.Model):
    seller=models.ForeignKey(User,on_delete=models.CASCADE)
    product_name=models.CharField(max_length=250)
    product_category=models.ForeignKey(course_type,on_delete=models.CASCADE)
    product_price=models.FloatField()
    sale_price=models.FloatField()
    product_image=models.ImageField(upload_to="products/")
    details=models.TextField()
class cart_course(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    topic=models.ForeignKey(course_type,on_delete=models.CASCADE)
    quantity=models.IntegerField()
    status=models.BooleanField(default=False)    
    added_on_DT=models.DateTimeField(auto_now_add=True,null=True) 
    updated_on=models.DateTimeField(auto_now=True,null=True) 
    def __str__(self):
        return self.user.username


class cart(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    product=models.ForeignKey(add_product,on_delete=models.CASCADE)
    quantity=models.IntegerField()
    status=models.BooleanField(default=False)    
    added_on_DT=models.DateTimeField(auto_now_add=True,null=True) 
    updated_on=models.DateTimeField(auto_now=True,null=True) 
    def __str__(self):
        return self.user.username  

class order(models.Model):
    cust_id=models.ForeignKey(User,on_delete=models.CASCADE)
    cart_ids=models.CharField(max_length=250)
    course_ids=models.CharField(max_length=250)
    invoice_id=models.CharField(max_length=250)
    status=models.BooleanField(default=False)    
    process_on=models.DateTimeField(auto_now_add=True) 
    def __str__(self):
        return self.cust_id.username  

class subscribe(models.Model):
    email_id=models.EmailField(max_length=250)
    def __str__(self):
        return self.email_id