"""doceo_django URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from doceo_app import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('paypal/', include('paypal.standard.ipn.urls')),
    path('admin/', admin.site.urls),
    path('',views.home,name="home"),
    path('about/',views.about,name="about"),
    
    path('contact/',views.contact,name="contact"),
    path('courses/',views.courses,name="courses"),
    
    path('courses_detail/',views.courses_detail,name="courses_detail"),
    path('stu_dashboard/',views.stu_dashboard,name="stu_dashboard"),
    path('tutor_dashboard/',views.tutor_dashboard,name="tutor_dashboard"),
    path('check_user/',views.check_user,name="check_user"),
    path('user_login/',views.user_login,name="user_login"),
    path('user_logout/',views.user_logout,name="user_logout"),
    path('start_course/',views.start_course,name="start_course"),
    
    
    path('tutor_courses/',views.tutor_courses,name="tutor_courses"),


    
    path('add_product/',views.add_product_view,name="add_product_view"),
    path('add_full_course/',views.add_full_course,name="add_full_course"),
    path('single_product/',views.single_product,name="single_product"),
    path('update_product/',views.update_product,name="update_product"),
    path('delete_product/',views.delete_product,name="delete_product"),
    path('all_courses/',views.all_courses,name="all_courses"),
    
    path('all_products/',views.all_products,name="all_products"),
    path('sendemail/',views.sendemail,name="sendemail"),
    path('reset_password/',views.reset_password,name="reset_password"),
    path('cart/',views.add_to_cart,name="cart"),
    path('cart_course_ss/',views.add_to_cart_course,name="cart_ss"),
    path('get_cart_data/',views.get_cart_data,name="get_cart_data"),
    path('change_quan/',views.change_quan,name="change_quan"),
    path('process_payment/',views.process_payment,name="process_payment"),
    path('payment_done/',views.payment_done,name="payment_done"),
    path('payment_cancelled/',views.payment_cancelled,name="payment_cancelled"),
    path('careers/',views.careers,name="careers"),
    path('my_courses/',views.my_courses,name="my_courses"),


    path('stu_dash_backup/',views.stu_dash_backup,name="stu_dash_backup"),
]+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
